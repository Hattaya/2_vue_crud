const professorService = {
  professorList: [
    {
      id: 1,
      name: 'ชัยชนะ สุดใจรัก',
      branch: 'CS',
      phoneNumber: '0861471058',
      email: 'chaichana@gmailcom'
    },
    {
      id: 2,
      name: 'เจิมขวัญ อยู่ยิ่ง',
      branch: 'CS',
      phoneNumber: '0812345678',
      email: 'chermkwan@gmailcom'
    }
  ],
  lastId: 3,
  addProfessor (data) {
    data.id = this.lastId++
    this.professorList.push(data)
  },
  updateProfessor (data) {
    const index = this.professorList.findIndex(item => item.id === data.id)
    this.professorList.splice(index, 1, data)
  },
  deleteProfessor (data) {
    const index = this.professorList.findIndex(item => item.id === data.id)
    this.professorList.splice(index, 1)
  },
  getProfessor () {
    return [...this.professorList]
  }
}

export default professorService
